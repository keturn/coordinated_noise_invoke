# Runs ruff, fixing any safely-fixable errors and formatting
ruff:
		ruff check . --fix
		ruff format .

# Runs ruff, fixing all errors it can fix and formatting
ruff-unsafe:
		ruff check . --fix --unsafe-fixes
		ruff format .
