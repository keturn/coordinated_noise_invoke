import secrets
import struct

import numpy as np


int_packing = struct.Struct("<i")


def morton_fill(
    position: np.ndarray, shape: np.ndarray, seed: int = None
) -> np.ndarray:
    """Fill array using Morton position encoding and FarmHash.

    :param position: the starting position of the region; coordinates will count up from here
    :param shape: shape of the array to fill with noise. (Assumed to be 3D.)
    :param seed: seeds the pseudorandom noise.
    :return: float16 array of the given shape
    """
    # local imports so we can make these dependencies optional
    import farmhash
    import pymorton
    import scipy

    if seed is None:
        seed = secrets.randbits(32)
    a = np.empty(shape, np.uint32)

    # we've imported these optimized functions in pymorton and farmhash
    # and then make them slow because pymorton doesn't work on numpy data types
    # and farmhash doesn't work on ints

    buf = bytearray(4)
    with np.nditer(a, flags=["multi_index"], op_flags=["writeonly"]) as it:
        for x in it:
            # Use morton encoding to convert our multidimensional coordinate to a single value to hash.
            # Is morton encoding better than just packing the inputs to sequential bytes? It shouldn't if
            # the following hash is good, but it seems like it would help resist streaks in rows or columns.
            point = position + it.multi_index
            z_order = pymorton.interleave3(*point.tolist())
            # convert the integer to bytes we can feed to the hash algorithm
            int_packing.pack_into(buf, 0, z_order)
            # I remember putting doing a fair bit of looking over various options before picking
            # farmhash, but do I remember why now? https://github.com/google/farmhash has since been
            # put in to read-only archive mode which makes this even more questionable.
            # The hash algorithm needn't be cryptographically secure, but we want something that we
            # can use on these inputs that are relatively small and close together, and we don't want
            # to see streaks or other patterns in even when we're only looking at 16 bits of the result.
            x[...] = farmhash.FarmHash32WithSeed(buf, seed)

    # Convert unsigned int32 uniform distribution to normal distribution
    return scipy.stats.norm.ppf(a / (1 << 32), 0).astype(np.float16)


def pcg64_fill(position: np.ndarray, shape: np.ndarray, seed: int = None) -> np.ndarray:
    """Fill array using numpy's PCG64.

    :param position: the starting position of the region; coordinates will count up from here
    :param shape: shape of the array to fill with noise. (Assumed to be 3D.)
    :param seed: seeds the pseudorandom noise.
    :return: float16 array of the given shape
    """
    if seed is None:
        seed = secrets.randbits(32)
    a = np.empty(shape, np.float16)
    seed = np.array([seed]).astype(np.uint32)
    with np.nditer(a, flags=["multi_index"], op_flags=["writeonly"]) as it:
        for x in it:
            point = position + it.multi_index
            # seed must consist of non-negative ints
            point_seed = np.append(seed, point).astype(np.uint32)
            rng = np.random.Generator(np.random.PCG64(point_seed))
            x[...] = rng.normal()

    return a


def test_morton_fill_is_normal():
    origin = np.array([0, 0, 0])
    shape = np.array([4, 64, 64])
    seed = 12345

    noise = morton_fill(origin, shape, seed)

    assert -0.1 < noise.mean() < 0.1, f"mean: {noise.mean()}"
    assert 0.9 < noise.std() < 1.1


def test_morton_fill_negative_offset_is_not_symmetrical():
    # making sure we didn't lose a bit and accidentally take the absolute value
    origin = np.array([0, 0, -3])
    shape = np.array([1, 1, 7])
    seed = 9876

    noise = morton_fill(origin, shape, seed)
    noise = noise[0][0].tolist()

    assert noise[2] != noise[4]


def test_pcg64_fill_is_normal():
    origin = np.array([0, 0, 0])
    shape = np.array([4, 64, 64])
    seed = 474747

    noise = pcg64_fill(origin, shape, seed)

    assert -0.1 < noise.mean() < 0.1, f"mean: {noise.mean()}"
    assert 0.9 < noise.std() < 1.1


if __name__ == "__main__":
    test_morton_fill_is_normal()
    test_morton_fill_negative_offset_is_not_symmetrical()
    test_pcg64_fill_is_normal()
    print("OK")
