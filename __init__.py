# Copyright 2023 Kevin Turner
# SPDX-License-Identifier: Apache-2.0
"""Coordinated Noise Generator.

The name and specific algorithm are original — that is, I just made them up. Please do let me know
if there's accepted terminology or existing best practices for this.
"""
from typing import Literal

import math

import numpy as np
import torch

from invokeai.invocation_api import (
    BaseInvocation,
    FieldDescriptions,
    InputField,
    InvocationContext,
    LatentsOutput,
    invocation,
)
from invokeai.app.invocations.noise import NoiseOutput
from invokeai.backend.util.devices import choose_torch_device, torch_dtype
from .noise import morton_fill, pcg64_fill

SEED_MAX = 0xFFFFFFFF  # FarmHash32 seed is uint32

algorithm_methods = {
    'pcg64': pcg64_fill,
    'morton+farmhash': morton_fill
}

FillAlgorithms = Literal[tuple(algorithm_methods.keys())]


@invocation(
    "noise_coordinated",
    title="Coordinated Noise",
    tags=["latents", "noise"],
    category="latents",
    use_cache=False,
    version="2.0.0",
)
class CoordinatedNoiseInvocation(BaseInvocation):
    """Generates latent noise that is stable for the given coordinates.

    That is, the noise at channel=1 x=3 y=4 for seed=42 will always be the same, regardless of the
    total size (width and height) of the region.

    It makes reproducible noise that you're able to crop or scroll.
    """

    _downsampling_factor = 8
    _latent_channels = 4

    seed: int = InputField(
        ge=0,
        le=SEED_MAX,
        description=FieldDescriptions.seed,
    )
    width: int = InputField(
        default=512,
        multiple_of=_downsampling_factor,
        gt=0,
        description=FieldDescriptions.width,
    )
    height: int = InputField(
        default=512,
        multiple_of=_downsampling_factor,
        gt=0,
        description=FieldDescriptions.height,
    )
    x_offset: int = InputField(
        default=0,
        description="x-coordinate of the lower edge",
        multiple_of=_downsampling_factor,
    )
    y_offset: int = InputField(
        default=0,
        description="y-coordinate of the lower edge",
        multiple_of=_downsampling_factor,
    )
    channel_offset: int = InputField(
        default=0, description="coordinate of the first channel"
    )
    algorithm: FillAlgorithms = InputField(
        default="pcg64", description="psuedo-random generation algorithm"
    )

    def invoke(self, context: InvocationContext) -> NoiseOutput:
        noise_tensor = self._make_tensor()
        name = context.tensors.save(tensor=noise_tensor)
        return NoiseOutput.build(
            latents_name=name, latents=noise_tensor, seed=self.seed
        )

    def _make_tensor(self) -> torch.tensor:
        origin = np.array(
            [self.channel_offset, self._offset_downsample(self.y_offset), self._offset_downsample(self.x_offset)]
        )
        shape = np.array(
            [self._latent_channels, self._dimension_downsample(self.height), self._dimension_downsample(self.width)]
        )
        fill = algorithm_methods[self.algorithm]
        np_noise = fill(origin, shape, self.seed)

        noise_tensor = torch.tensor(np_noise, dtype=torch_dtype(choose_torch_device()))
        noise_tensor.unsqueeze_(0)  # batch size of 1
        return noise_tensor

    def _offset_downsample(self, x: int) -> int:
        return x // self._downsampling_factor

    def _dimension_downsample(self, x: int) -> int:
        return x // self._downsampling_factor


@invocation(
    "noise_coordinated_flux",
    title="Coordinated Noise (Flux)",
    tags=["latents", "noise"],
    category="latents",
    use_cache=False,
    version="2.0.0",
)
class CoordinatedFluxNoiseInvocation(CoordinatedNoiseInvocation):
    """Generates latent noise that is stable for the given coordinates.

    That is, the noise at channel=1 x=3 y=4 for seed=42 will always be the same, regardless of the
    total size (width and height) of the region.

    It makes reproducible noise that you're able to crop or scroll.

    This variant has 16 channels for Flux.
    """
    _downsampling_factor = 8
    _latent_channels = 16

    def _dimension_downsample(self, x: int) -> int:
        # factor pixels-per-latent, but latent must have even dimensions? according to math in get_noise
        return 2 * math.ceil(x / self._downsampling_factor / 2.0)

    def invoke(self, context: InvocationContext) -> LatentsOutput:
        noise_tensor = self._make_tensor()
        name = context.tensors.save(tensor=noise_tensor)
        # Flux node only takes a Latent, not a different Noise type.
        return LatentsOutput.build(
            latents_name=name, latents=noise_tensor, seed=self.seed
        )
